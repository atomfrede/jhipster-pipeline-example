/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gitlab.atomfrede.jhipster.pipeline.example.web.rest.vm;
